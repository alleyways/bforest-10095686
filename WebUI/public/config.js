window.config = {
    message: 'message text here',
    title: 'Black Forest - Demo Project',
    apiurl: 'http://localhost/mobforest',
    itemConnection: 'mo-timestep',
    featureConnection: 'mo-gis',
    timeseriesConnection: 'mo-timeseries',
    timeseriesDirectConnection: 'mc-timeseries',
    spreadSheetConnection: 'mc-spreadsheet',
    documentConnection: 'mc-doc',
    appsubdirectory: '/bforestui',
    roles: {
        Home: [],
        MineMap: [],
        Assets: [],
        Data: ['Editor', 'Administrator'],
        GoldSim: [],
    }

}