window.config = {
    message: 'message text here',
    title: 'Black Forest - Demo Project',
    apiurl: 'https://dhi.mike.mine.bz/mobforest',
    itemConnection: 'mo-timestep',
    featureConnection: 'mo-gis',
    timeseriesConnection: 'mo-timeseries',
    timeseriesDirectConnection: 'mc-timeseries',
    spreadSheetConnection: 'mc-spreadsheet',
    appsubdirectory: '/bforestui',
    roles: {
        Home: ['User', 'Editor', 'Administrator'],
        MineMap: ['Administrator'],
        Assets: ['Editor', 'Administrator'],
        Data: ['Editor', 'Administrator'],
        GoldSim: ['Administrator'],
    }

}