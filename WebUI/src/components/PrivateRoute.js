import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom'
import { UserContext } from '../providers/UserContext'

function withPrivateRoute (WrappedComponent) {

    return class extends Component {
        render() {
            return (
                <UserContext.Consumer>
                    {({currentUser, login}) => (
                        currentUser == null
                         ? <Redirect to={{
                            pathname: "/login",
                            state: { referrer: this.props.location.pathname }
                          }} />
                         : <Route {...this.props} component={WrappedComponent}/>
                    )}
                </UserContext.Consumer>
            );
        }
    }    
}

export { withPrivateRoute };