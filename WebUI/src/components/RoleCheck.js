import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom'

import { UserContext } from '../providers/UserContext'

function withRoleCheck (WrappedComponent, Roles) {

    return class extends Component {
       
        render() {                        
            
            return (
                <UserContext.Consumer>
                    {({currentUser}) => {

                        const requested = this.props.location.pathname === '/'
                            ? 'home'
                            : this.props.location.pathname

                        const roleKey = Object.keys(Roles).filter(page => {
                            return requested.toLowerCase().includes(page.toLowerCase())
                        })[0]

                        const pageRoles = Roles[roleKey]
                        
                        // when serialization fixed, change back to user.roles
                        const userRoles = currentUser === null
                            ? ['Guest']
                            : currentUser.roles

                        const hits = userRoles.filter(role => {
                            return pageRoles.includes(role)
                        })

                        // no authorization required case
                        if (pageRoles.length === 0) {
                            return <Route {...this.props} component={WrappedComponent}/>
                        }

                        // authentication required case
                        if (currentUser === null && hits.length === 0) {
                            return <Redirect
                                        to={{
                                            pathname: "/login",
                                            state: { referrer: this.props.location.pathname }
                                        }} />
                        }

                        // authenticated but not authorized case
                        if (currentUser !== null && hits.length === 0) {
                            return <Redirect
                                        to={{
                                            pathname: "/yourrole",
                                            state: { referrer: this.props.location.pathname }
                                        }} />
                        }

                        // authenticated and authorized case
                        if (currentUser !== null && hits.length > 0) {
                            return <Route {...this.props} component={WrappedComponent}/>
                        }
                                                      
                        console.error('this should not be reached')   
                        return <div>roles error</div>
                    }
                }
                </UserContext.Consumer>                
            );
        }
    }    
}

export { withRoleCheck };