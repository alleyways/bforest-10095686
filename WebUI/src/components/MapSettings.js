import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import PublicOutlinedIcon from '@material-ui/icons/PublicOutlined';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import IconButton from '@material-ui/core/IconButton';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const styles = theme => ({
    root: {
        fontFamily: theme.fontFamily,
        overflowX: 'hidden',
        overflowY: 'auto',
    },
    heading: {
        color: theme.palette.dhi.text,
        fontWeight: 700,
        fontSize: '16px',
        padding: theme.spacing.unit * 2,
    },
    groupHeading: {
        color: theme.palette.dhi.text,
        fontWeight: 700,
        fontSize: '16px',
        flexGrow: 1,
    },
    layerItem: {
        color: theme.palette.dhi.text,
        fontSize: '16px',
        padding: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'row',
        fontWeight: 100
    },
    layerIcon: {
        marginRight: theme.spacing.unit * 2,
    },
    layerLabel: {
        flexGrow: 1,
        cursor: 'pointer'
    },
    iconButton: {
        padding: 3,
        marginRight: 16
    },
    expandIcon: {
        paddingRight: '0'
    }
});

const ExpansionPanel = withStyles({
    root: {
      boxShadow: 'none',
      '&:not(:last-child)': {
        borderBottom: 0,
      },
      '&:before': {
        display: 'none',
      },
      '&$expanded': {
        margin: 'auto',
      },
    },
    expanded: {},
})(MuiExpansionPanel);
  
const ExpansionPanelSummary = withStyles({
    root: {
      paddingRight: 0,
      backgroundColor: 'rgba(0, 0, 0, .01)',
      minHeight: 56,
      '&$expanded': {
        minHeight: 56,
      },
    },
    content: {
      '&$expanded': {
        margin: '12px 0',
      },      
    },    
    expanded: {},
})(MuiExpansionPanelSummary);
  
const ExpansionPanelDetails = withStyles(theme => ({
    root: {
      padding: 0,
      flexDirection: 'column'
    },
}))(MuiExpansionPanelDetails);

class MapSettings extends Component{

    constructor(props) {
        super(props)
        this.state = {
            expanded: []
        }
    }

    // two fat arrows is function currying, where this is the same as
    // additional args to the event handler are supplied to the outer func
    // function(panel){
    //     this.function(event, expanded) {
    //          ... 
    //     }.bind(this)
    // }.bind(this)
    handleChange = panel => (event, newExpanded) => {
        this.setState(state => {
            // if in, remove it
            if (state.expanded.includes(panel))
            {
                let index = state.expanded.indexOf(panel)
                const list = state.expanded.slice()
                list.splice(index, 1)
                return { expanded: list }
            }

            // if out, insert it
            const list = state.expanded.slice()
            list.push(panel)
            return { expanded: list }
        });
    };

    showPanel = panel => {
        return this.state.expanded.includes(panel)
    }
    
    render() {
        const { classes } = this.props

        const items = this.props.settings.map((item) =>
            <React.Fragment key={item.category} >
                <ExpansionPanel square expanded={this.showPanel(item.category)} onChange={this.handleChange(item.category)}>
                    <ExpansionPanelSummary aria-controls={item.category} id={item.category}>
                        <div className={classes.groupHeading}>{item.category}</div>
                        <div>
                            { 
                                this.showPanel(item.category) ?
                                <ExpandLessIcon /> :
                                <ExpandMoreIcon />
                            }
                        </div>
                    </ExpansionPanelSummary>
                    <Divider />
                    <ExpansionPanelDetails>
                        {item.layers.map((layer) => 
                            <React.Fragment key={layer.name}>
                                <div className={classes.layerItem}>
                                    {layer.icon !== '' ? <PublicOutlinedIcon className={classes.layerIcon} /> : false }                                                                
                                    <div onClick={() => this.props.onLabelClick(item, layer)} className={classes.layerLabel}>{layer.name}</div>
                                    {
                                        <IconButton className={classes.iconButton} onClick={() => this.props.onEyeClick(item, layer)}>
                                            {
                                                layer.visible ? 
                                                <VisibilityOutlinedIcon /> : 
                                                <VisibilityOffOutlinedIcon />
                                            }
                                        </IconButton>                                
                                    }  
                                </div>
                                {/* <Divider /> */}
                            </React.Fragment>
                        )}
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <Divider />
            </React.Fragment>
        );

        return (
            <div className={classes.root}>
                <Divider />
                <div className={classes.heading}>Map Settings</div>
                <Divider />
                {items}
            </div>
        )
    }
}

export default withStyles(styles)(MapSettings)