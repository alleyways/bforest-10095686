import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom'
import { Toolbar } from '@material-ui/core';
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import PublicOutlinedIcon from '@material-ui/icons/PublicOutlined';
import ListOutlinedIcon from '@material-ui/icons/ListOutlined';

// muitheme, theme variable is injected and can be used to customise
const styles = theme => ({
  flex: {
      flexGrow: 1,
  },
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    textAlign: 'center',
  },
  nav: {
    textDecoration: 'none',
    color: theme.palette.dhi.text,
    fontWeight: 700,
    fontSize: '16px',
    fontFamily: theme.fontFamily,
    lineHeight: '25px',
  },
  selected: {
      borderBottomWidth: '4px',
      borderBottomColor: theme.palette.dhi.text,
      borderBottomStyle: 'solid',
  }  
});

class TabBar extends Component {
    render() {
        const { classes } = this.props
        
        return (
            <Toolbar>
                <div className={classes.flex} />
                <div className={[classes.flex, classes.tabContainer].join(' ')}>
                    <div className={classes.flex} />
                    <NavLink exact to="/" activeClassName={classes.selected} className={[classes.flex, classes.nav].join(' ')}>
                        <div className={classes.tabContainer}>
                            <div className={classes.flex} />
                            <DashboardOutlinedIcon />
                            <div>Home</div>
                            <div className={classes.flex} />
                        </div>
                    </NavLink>
                    <NavLink to="/minemap" activeClassName={classes.selected} className={[classes.flex, classes.nav].join(' ')}>
                        <div className={classes.tabContainer}>
                            <div className={classes.flex} />
                            <PublicOutlinedIcon />
                            <div>Mine Map</div>
                            <div className={classes.flex} />
                        </div>
                    </NavLink>
                    <NavLink to="/assets" activeClassName={classes.selected} className={[classes.flex, classes.nav].join(' ')}>
                        <div className={classes.tabContainer}>
                            <div className={classes.flex} />
                            <ListOutlinedIcon />
                            <div>Assets</div>
                            <div className={classes.flex} />
                        </div>
                    </NavLink>
                    <NavLink to="/data" activeClassName={classes.selected} className={[classes.flex, classes.nav].join(' ')}>
                        <div className={classes.tabContainer}>
                            <div className={classes.flex} />
                            <ListOutlinedIcon />
                            <div>Data</div>
                            <div className={classes.flex} />
                        </div>
                    </NavLink>
                    <NavLink to="/goldsim" activeClassName={classes.selected} className={[classes.flex, classes.nav].join(' ')}>
                        <div className={classes.tabContainer}>
                            <div className={classes.flex} />
                            <ListOutlinedIcon />
                            <div>GoldSim</div>
                            <div className={classes.flex} />
                        </div>
                    </NavLink>
                    <div className={classes.flex} />
                </div>
                <div className={classes.flex} />
            </Toolbar>
        );
    }
}

export default withStyles(styles)(TabBar)