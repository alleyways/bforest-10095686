import { createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

const DHITheme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  fontFamily: 'Roboto',
  palette: {
    primary: { main: '#F2F5F7' },
    secondary: { main: '#3d6079' },
    error: red,
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    dhi: { text: '#0B4566', success: green[500]}
  },
});

export default DHITheme;
