import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { NavLink } from 'react-router-dom'
import { UserContext } from '../providers/UserContext'

import dhilogo from '../webassets/logo.png'

// muitheme, theme variable is injected and can be used to customise
const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  spacer: {
    flexGrow: 1,
  },
  appBarLink: {
    textDecoration: 'none',
    color: theme.palette.dhi.text,
    fontWeight: 700,
    fontSize: '16px',
    fontFamily: theme.fontFamily
  },
  logo: {
      height: '40px',
      marginRight: theme.spacing.unit * 2,
  }
});

class TopAppBar extends Component {
    render() {
        const { classes, title } = this.props

        return (
            <AppBar position="static" className={classes.appBar}>
              <Toolbar>
                  <img className={classes.logo} src={dhilogo} alt="Logo" />
                  <div className={classes.appBarLink}>{title}</div>
                  <div className={classes.spacer} />
                  <UserContext.Consumer>
                      {({currentUser}) => (
                          currentUser === null
                          ? <NavLink to="/login" className={classes.appBarLink}>Log in</NavLink>
                          : <NavLink to="/logoff" className={classes.appBarLink}>Log off</NavLink>
                      )}
                  </UserContext.Consumer>                  
              </Toolbar>
            </AppBar>
        );
    }
}

export default withStyles(styles)(TopAppBar)