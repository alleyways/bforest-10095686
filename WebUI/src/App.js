import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter, Route } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core/styles';

import TopAppBar from './components/TopAppBar';
import TabBar from './components/TabBar';
import { withRoleCheck } from './components/RoleCheck';
import Home from './pages/home';
import MineMap from './pages/minemap';
import Assets from './pages/assets';
import Data from './pages/data';
import GoldSim from './pages/goldsim';
import LoginPage from './pages/login';
import Logoff from './pages/logoff';
import YourRole from './pages/yourrole';
import DHITheme from './components/DHITheme';
import { UserContext } from './providers/UserContext';

const theme = DHITheme;

// The app root div uses flexbox column so 
// all children are rows stretched to full screen width
const styles = () => ({
  root: {
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
  },
});

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      currentUser: null,
      login: this.login,
      logoff: this.logoff,
    }
  }

  login = user => {
      this.setState(state => {
        return { currentUser: user }
      });
  };

  logoff = () => {
    this.setState(state => {
      return { currentUser: null }
    });
  };

  render() {
    const { classes } = this.props
    const roles = window.config.roles
    
    return (
      <div className={classes.root}>
        <CssBaseline />
        <UserContext.Provider value={this.state}>
          <MuiThemeProvider theme={theme}>
            <BrowserRouter basename={window.config.appsubdirectory}>
              <TopAppBar title={window.config.title}/>
              <TabBar />
              <Route exact path="/" component={withRoleCheck(Home, roles)} />
              <Route path="/minemap" component={withRoleCheck(MineMap, roles)} />
              <Route path="/assets" component={withRoleCheck(Assets, roles)} />
              <Route path="/data" component={withRoleCheck(Data, roles)} />
              <Route path="/goldsim" component={withRoleCheck(GoldSim, roles)} />
              <Route path="/yourrole" component={YourRole} />
              <Route path="/login" component={LoginPage} />
              <Route path="/logoff" component={Logoff} />
            </BrowserRouter>
          </MuiThemeProvider>
        </UserContext.Provider>   
      </div>
    );
  }
}

export default withStyles(styles)(App);