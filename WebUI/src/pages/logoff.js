import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

import { UserContext } from '../providers/UserContext'

class Logoff extends Component {

    performLogoff = logoffFunc => {
        logoffFunc()
        return <Redirect to='/'/>
    }

    render() {        
        return (
            <UserContext.Consumer>
                {({logoff}) => (
                    this.performLogoff(logoff)
                )}
            </UserContext.Consumer>               
        )        
    }
}

export default Logoff;