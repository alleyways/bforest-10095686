import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Redirect } from 'react-router-dom'

import { UserContext } from '../providers/UserContext';
import { Login } from '../dhi/Login'

// Home panel uses flexbox column so
// all children are columns stretched to full height
const styles = theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        padding: theme.spacing.unit * 2
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '100%',
        display: 'flex',
    },
    logincontainer: {
        maxWidth: '300px',
        display: 'flex',
        flexDirection: 'column'
    },
    flex: {
        flexGrow: 2,
    },
});

class LoginPage extends Component {

    render() {
        const { classes, location } = this.props;
        const referrer = location.state ? location.state.referrer : '/'

        return (
            <UserContext.Consumer>
                {({currentUser, login}) => (

                    currentUser === null
                        ? <div className={classes.root}>
                                <Paper className={classes.paper}>
                                    <div className={classes.flex} />
                                    <div className={classes.logincontainer}>
                                        <div className={classes.flex} />
                                        <Login 
                                            host={window.config.apiurl}
                                            onLogin={login}
                                            loginButtonText="Login"
                                            userNamePlaceholder="Username"
                                            passwordPlaceholder="Password"
                                            resetPasswordButtonText="Reset Password"
                                            resetPasswordUserNamePlaceholder="E-Mail Address or User ID"
                                        />
                                        <div className={classes.flex} />
                                    </div>
                                    <div className={classes.flex} />
                                </Paper>
                            </div>
                        : <Redirect to={referrer} />
                )}
            </UserContext.Consumer>            
        )
    }
}

export default withStyles(styles)(LoginPage)