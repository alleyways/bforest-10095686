import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
        fontFamily: theme.fontFamily,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '100%',
    },
    nav: {
        textDecoration: 'none',
        color: theme.palette.dhi.text,
        fontWeight: 700,
        fontSize: '16px',
        fontFamily: theme.fontFamily,
        lineHeight: '25px',
    },
});

class YourRole extends Component {
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    Your user account roles are not sufficient for the requested page
                </Paper>            
            </div>            
        )
    }
}

export default withStyles(styles)(YourRole)