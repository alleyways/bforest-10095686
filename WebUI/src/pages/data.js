import React, { Component } from 'react';
import { withStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress'
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { blue, red, green, lightBlue } from '@material-ui/core/colors';
import Paper from '@material-ui/core/Paper';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import MomentUtils from '@date-io/moment';
import moment from 'moment/moment';
import { DateTimePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MUIDataTable from 'mui-datatables';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import Parse from 'csv-parse'
import { UserContext } from '../providers/UserContext'
import { fetchSpreadsheetUsedRange, fetchTimeseriesValues, updateTimeseries } from '../dhi/DataServices';
import csv_import_sample from '../webassets/csv_import_sample.csv'
const ChartPlotly = React.lazy(() => import('../dhi/Charts/ChartPlotly'));

// Home panel uses flexbox column so
// all children are columns stretched to full height
const styles = theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
        fontFamily: theme.fontFamily,
    },
    paper: {
        display: 'flex',
        flexDirection: 'row',
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary,
        width: '100%',
    },
    controlArea: {
        display: 'flex',
        flexDirection: 'column',
        minWidth: 660,
        width: '100%'
    },
    controlRow: {
        display: 'flex',
        flexDirection: 'row',
        padding: theme.spacing.unit * 2,
    },
    dataArea: {
        flexGrow: '1',
        display: 'flex',
        flexDirection: 'row',
    },
    button: {
        marginLeft: 20,
    },
    flex: {
        flexGrow: 1,
    },
    spacer: {
        width: 50,
    },
    missingData: {
        flexGrow: 1,
        textAlign: 'center',
        paddingTop: 50,
    },
    tableArea: {
        flexGrow: 0,
    },
    chartArea: {
        flexGrow: 1,
        marginLeft: '10px',
    },
    missingContainer: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    formControl: {
        width: 220,
    },
    progress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        zIndex: 999
    },
    csv: {
        textDecoration: 'none',
        color: theme.palette.dhi.text,
        fontWeight: 700,
        fontSize: '16px',
        fontFamily: theme.fontFamily,
        lineHeight: '36px',
        marginRight: '40px',
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    success: {
        backgroundColor: theme.palette.dhi.success,
    },

});

const pickerTheme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    fontFamily: 'Roboto',
    palette: {
        primary: { main: '#F2F5F7' },
        secondary: { main: '#3d6079' },
        error: red,
        // Used by `getContrastText()` to maximize the contrast between the background and
        // the text.
        contrastThreshold: 3,
        // Used to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
        dhi: { text: '#0B4566', success: green[500] }
    },
    overrides: {
        MuiPickersToolbar: {
            toolbar: {
                backgroundColor: '#0B4566',
            },
        },
        MuiPickersClockPointer: {
            pointer: {
                backgroundColor: '#0B4566',
            },
            thumb: {
                border: '14px solid #0B4566'
            }
        },
        MuiPickersClockNumber: {
            clockNumber: {
                color: '#0B4566',
            }
        },
        MuiPickersCalendarHeader: {
            switchHeader: {
                backgroundColor: '#F2F5F7',
                color: '#0B4566',
            },
            iconButton: {
                color: '#0B4566',
            }
        },
        MuiPickerDTTabs: {
            tabs: {
                color: '#0B4566',
            }
        },
        MuiPickersDay: {
            day: {
                color: '#0B4566',
            },
            isSelected: {
                backgroundColor: '#0B4566',
            },
            current: {
                color: lightBlue["900"],
            },
        },
        MuiPickersModal: {
            dialogAction: {
                color: '#0B4566',
            },
        },
    }
})

const DataTable = withStyles({
    responsiveScroll: {
        height: 'calc(100vh - 580px)',
        maxHeight: 'none',
    },
})(MUIDataTable);


class Data extends Component {

    constructor(props) {
        super(props)
        this.state = {
            assetType: '',
            timeseriesName: '',
            fromDate: null,
            toDate: null,
            loading: [],
            snackOpen: false,
            snackMessage: '',
            snackVariant: '',
            fromHelper: '',
            toHelper: '',
            spreadsheetConfig: [],
            timeseriesData: [],
            chartLayout: {
                autosize: true,
                showlegend: false,
                title: '',
                margin: {
                    b: 30,
                    t: 30,
                },
                xaxis: {
                    title: false
                },
            },
        }
    }

    handleDateChange = which => e => {
        const newDate = e ? moment(e._d) : null
        this.setState(state => ({ [which]: newDate }),
            () => this.fetchData())
    }

    handleTableUpdate = (meta, func) => event => {
        const updatedData = Object.assign([], this.state.timeseriesData)
        updatedData[meta.rowIndex][meta.columnIndex] = event.target.value
        this.setState(state => ({ timeseriesData: updatedData }))
    }

    handleSelect = (e) => {
        const stateResult = { [e.target.name]: e.target.value }

        // blank out timeseriesName when changing assetType
        if (e.target.name === 'assetType') {
            stateResult.timeseriesName = ''
        }

        this.setState(state => (stateResult),
            () => this.fetchData())
    }

    handleSpreadsheet = data => {
        this.setState(state => ({ spreadsheetConfig: data }))
    }

    handleFileRead = e => {
        const content = e.target.result
        const options = {
            trim: true,
            from_line: 2,
        }

        Parse(content, options, (err, output) => {
            if (err) {
                this.setState(state => ({
                    snackMessage: 'Error parsing CSV',
                    snackOpen: true,
                    snackVariant: 'error'
                }))

                return;
            }

            // try to update the timeseriesData state array
            const updatedData = Object.assign([], this.state.timeseriesData)
            output.forEach(csvrow => {
                updatedData.push([csvrow[0], 0, csvrow[1]])
            });
            this.setState(state => ({ timeseriesData: updatedData }))
        })
    }

    handleImportClick = file => {
        const fileReader = new FileReader()
        fileReader.onloadend = this.handleFileRead
        fileReader.readAsText(file)
    }

    handleSaveClick = user => {
        const { spreadsheetConfig, assetType, timeseriesName, timeseriesData, } = this.state;

        const tsPath = spreadsheetConfig
            .filter(r => { return r[0] === assetType && r[1] === timeseriesName })[0][2]

        // format the timeseriesData
        const dateTimes = timeseriesData.map(d => {
            return d[0]
        })

        const values = timeseriesData.map(d => {
            return d[2] !== '' ? d[2] : d[1]
        })

        const data = {
            DateTimes: dateTimes,
            Values: values
        }

        this.addLoader()
        updateTimeseries(
            window.config.apiurl,
            window.config.timeseriesDirectConnection,
            user,
            tsPath,
            data
        ).subscribe(
            null,
            () => this.removeLoader('Error updating timeseries'),
            () => { this.removeLoader(); this.showSuccess(); this.fetchData() }
        );
    }

    handleAdd = e => {
        const updatedData = Object.assign([], this.state.timeseriesData)
        const lastDate = updatedData[updatedData.length - 1][0]
        const newDate = moment(lastDate).add(1, 'hours')

        updatedData.push([newDate.format('YYYY-MM-DDTHH:mm:ss'), 0, ''])
        this.setState(state => ({ timeseriesData: updatedData }))
    }

    handleTimeseries = data => {
        if (data.length !== 1) {
            this.setState(state => ({ timeseriesData: [] }))
            return
        }

        const dataWithEditable = data[0].data.map(d => {
            return [d[0], d[1], '']
        })

        this.setState(state => ({ timeseriesData: dataWithEditable }))
    }

    fetchData = () => {
        // check selectors and fetch timeseries data
        // warn the user that edits will be lost?
        const { spreadsheetConfig, assetType, timeseriesName, fromDate, toDate } = this.state;

        if (assetType === '' || timeseriesName === '') {
            this.setState(state => ({ timeseriesData: [] }))
            return
        }

        const tsPath = spreadsheetConfig
            .filter(r => { return r[0] === assetType && r[1] === timeseriesName })[0][2]

        let dataSource = {
            host: window.config.apiurl,
            connection: window.config.timeseriesDirectConnection,
            ids: [
                tsPath,
            ],
        }

        if (fromDate !== null) {
            dataSource.from = fromDate.format('YYYY-MM-DDTHH:mm:ss')
        }

        if (toDate !== null) {
            dataSource.to = toDate.format('YYYY-MM-DDTHH:mm:ss')
        }

        this.addLoader()
        fetchTimeseriesValues([dataSource]).subscribe(
            data => this.handleTimeseries(data),
            () => this.removeLoader('Error fetching timeseries.'),
            () => this.removeLoader()
        );
    }

    addLoader = () => {
        const loading = Object.assign([], this.state.loading)
        loading.push('loading')
        this.setState(state => ({ loading: loading }))
    }

    removeLoader = (errorMessage = null) => {
        const loading = Object.assign([], this.state.loading)
        loading.pop()
        this.setState(state => ({ loading: loading }))

        if (errorMessage) {
            this.setState(state => ({
                snackMessage: errorMessage,
                snackOpen: true,
                snackVariant: 'error'
            }))
        }
    }

    showSuccess = () => {
        this.setState(state => ({
            snackMessage: 'Successfully updated',
            snackOpen: true,
            snackVariant: 'success'
        }))
    }

    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            snackOpen: false
        });
    }

    componentDidMount () {
        // fetch the MO spreadsheet to populate the selectors
        this.addLoader()
        fetchSpreadsheetUsedRange(
            window.config.apiurl,
            window.config.spreadSheetConnection,
            'Configuration', 'WebAppData')
            .subscribe(
                data => this.handleSpreadsheet(data),
                () => this.removeLoader('Error fetching spreadsheet'),
                () => this.removeLoader()
            );
    }

    formatTimeseriesToPlotly = data => {

        if (data.length === 0) {
            return []
        }

        // plotly styling rules:
        // https://images.plot.ly/plotly-documentation/images/plotly_js_cheat_sheet.pdf
        const trace = {
            data: {
                DateTimes: data.map(pair => pair[0]),
                Values: data.map(pair => pair[1])
            },
        }

        return [trace]
    }

    fallback = () => {
        return (
            <div>Loading...</div>
        );
    }

    render () {
        const { classes } = this.props

        const {
            loading,
            spreadsheetConfig,
            assetType,
            timeseriesData,
            snackOpen,
            snackMessage,
            snackVariant,
            chartLayout,
        } = this.state;

        // reformat timeseriesdata for plotly
        const plotlyData = this.formatTimeseriesToPlotly(timeseriesData)

        const assetTypes = [...new Set(spreadsheetConfig.map(s => s[0]))]
        const tsList = assetType === '' ? [] : spreadsheetConfig
            .filter(s => { return s[0] === assetType })
            .map(t => t[1])

        const columns = [
            {
                label: 'Time',
                options: {
                    filter: false,
                    sort: false,
                }
            },
            {
                label: 'Current Value',
                options: {
                    filter: false,
                    sort: false,
                }
            },
            {
                label: 'New Value',
                options: {
                    filter: false,
                    sort: false,
                    customBodyRender: (value, tableMeta, updateValue) => (
                        <Input
                            value={value || ''}
                            onChange={this.handleTableUpdate(tableMeta, updateValue)} />
                    ),
                }
            },
        ]

        // note the toolbar is hidden in DHITheme.js override
        const options = {
            filterType: 'dropdown',
            selectableRows: 'none',
            rowHover: false,
            rowsPerPage: 10,
            responsive: 'scroll',
            sort: false,
            filter: false,
            search: false,
            print: false,
            download: false,
            viewColumns: false,
            customToolbar: () => {
                return (
                    <React.Fragment>
                        <Tooltip title={'Add row to end'}>
                            <IconButton onClick={this.handleAdd}>
                                <AddIcon />
                            </IconButton>
                        </Tooltip>
                    </React.Fragment>
                );
            }
        }

        const progress = loading.length > 0 && (
            <CircularProgress className={classes.progress} />
        )

        const table = timeseriesData.length > 0 ? (
            <div className={classes.dataArea}>
                <div className={classes.tableArea}>
                    <DataTable
                        data={timeseriesData}
                        columns={columns}
                        options={options} />
                </div>
                <div className={classes.chartArea}>
                    <React.Suspense fallback={this.fallback()}>
                        <ChartPlotly
                            timeseries={plotlyData}
                            data={plotlyData}
                            layout={chartLayout} />
                    </React.Suspense>
                </div>
            </div>
        ) : (<div className={classes.missingContainer}>
            <div className={classes.missingData}>No data for selected period</div>
            <div className={classes.flex} />
        </div>)

        const buttons = timeseriesData.length > 0 && (
            <UserContext.Consumer>
                {({ currentUser }) => (
                    <React.Fragment>
                        <a className={classes.csv} href={csv_import_sample} download="csv_import_sample.csv">Download Sample CSV</a>
                        <input
                            accept='.csv'
                            className={classes.input}
                            style={{ display: 'none' }}
                            id='file'
                            type='file'
                            onChange={e => this.handleImportClick(e.target.files[0])}
                        />
                        <label htmlFor='file'>
                            <Button variant='contained' component='span' className={classes.button}>
                                Import CSV
                                </Button>
                        </label>
                        <Button
                            variant='contained'
                            onClick={() => this.handleSaveClick(currentUser)}
                            className={classes.button}>
                            Save
                            </Button>
                    </React.Fragment>
                )}
            </UserContext.Consumer>
        )

        return (
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <div className={classes.root}>
                    {progress}
                    <Paper className={classes.paper}>
                        <div className={classes.controlArea}>
                            <div className={classes.controlRow}>
                                <div className={classes.flex} />
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor='assetType'>Asset Type</InputLabel>
                                    <Select
                                        value={this.state.assetType}
                                        onChange={this.handleSelect}
                                        inputProps={{
                                            name: 'assetType',
                                            id: 'assetType',
                                        }}>
                                        {assetTypes.map((a) =>
                                            <MenuItem key={a} value={a}>{a}</MenuItem>
                                        )}
                                    </Select>
                                </FormControl>
                                <div className={classes.spacer} />
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor='timeseriesSelect'>Timeseries</InputLabel>
                                    <Select
                                        value={this.state.timeseriesName}
                                        onChange={this.handleSelect}
                                        inputProps={{
                                            name: 'timeseriesName',
                                            id: 'timeseriesName',
                                        }}>
                                        {tsList.map((t) =>
                                            <MenuItem key={t} value={t}>{t}</MenuItem>
                                        )}
                                    </Select>
                                </FormControl>
                                <div className={classes.flex} />
                            </div>
                            <div className={classes.controlRow}>
                                <div className={classes.flex} />
                                <FormControl className={classes.formControl}>
                                    <MuiThemeProvider theme={pickerTheme}>
                                        <DateTimePicker
                                            helperText={this.state.fromHelper}
                                            error={false}
                                            keyboard
                                            clearable
                                            label='From'
                                            value={this.state.fromDate}
                                            onChange={this.handleDateChange('fromDate')} />
                                    </MuiThemeProvider>
                                </FormControl>
                                <div className={classes.spacer} />
                                <FormControl className={classes.formControl}>
                                    <MuiThemeProvider theme={pickerTheme}>
                                        <DateTimePicker
                                            helperText={this.state.toHelper}
                                            error={false}
                                            keyboard
                                            clearable
                                            label='To'
                                            value={this.state.toDate}
                                            onChange={this.handleDateChange('toDate')} />
                                    </MuiThemeProvider>
                                </FormControl>
                                <div className={classes.flex} />
                            </div>
                            {table}
                            <div className={classes.controlRow}>
                                <div className={classes.flex} />
                                {buttons}
                            </div>
                        </div>
                    </Paper>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={snackOpen}
                        onClose={this.handleSnackClose}
                        autoHideDuration={6000}>
                        <SnackbarContent
                            className={classes[snackVariant]}
                            aria-describedby='client-snackbar'
                            message={
                                <span id='client-snackbar' className={classes.message}>
                                    {snackMessage}
                                </span>
                            }
                            onClose={this.handleSnackClose}
                        />
                    </Snackbar>
                </div>
            </MuiPickersUtilsProvider>
        )
    }
}

export default withStyles(styles)(Data)