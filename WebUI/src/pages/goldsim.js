import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { blue } from '@material-ui/core/colors';
import { fetchSpreadsheetUsedRange, uploadDocument } from '../dhi/DataServices';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from '../providers/UserContext'

const styles = theme => ({
    root: {
        display: 'flex',
        padding: theme.spacing.unit * 2,
        flexGrow: 1,
        flexDirection: 'column',
        fontFamily: theme.fontFamily,
    },
    goldsimArea: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        color: theme.palette.dhi.text,
        padding: theme.spacing.unit * 2,
    },
    progress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        zIndex: 999
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
})

class Assets extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: [],
            snackOpen: false,
            snackMessage: '',
            snackVariant: '',
            data: [],
        }
    }
    componentDidMount () {
        this.addLoader();
        fetchSpreadsheetUsedRange(
            window.config.apiurl,
            window.config.spreadSheetConnection,
            'Configuration|5 WebApp', 'GoldSim runs')
            .subscribe(
                data => this.handleSpreadsheet(data),
                () => this.removeLoader('Error fetching spreadsheet'),
                () => this.removeLoader()
            );
    }
    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            snackOpen: false
        });
    }
    addLoader = () => {
        const loading = Object.assign([], this.state.loading)
        loading.push('loading')
        this.setState(state => ({
            loading: loading
        }))
    }
    removeLoader = (errorMessage = null) => {
        const loading = Object.assign([], this.state.loading)
        loading.pop()
        this.setState(state => ({
            loading: loading
        }))

        if (errorMessage) {
            this.setState(state => ({
                snackMessage: errorMessage,
                snackOpen: true,
                snackVariant: 'error'
            }))
        }
    }
    handleSpreadsheet = data => {
        this.setState(state => ({ data: data }));
    }
    uploadFile = (currentUser, e) => {
        uploadDocument(
            window.config.apiurl,
            window.config.documentConnection,
            currentUser,
            e.currentTarget.files[0]);
    }
    render () {
        const { classes } = this.props
        const {
            snackOpen,
            snackMessage,
            snackVariant,
            loading,
            data
        } = this.state
        const progress = loading.length > 0 && (
            <CircularProgress className={classes.progress} />
        )
        const button = (
            <UserContext.Consumer>
                {({ currentUser }) => currentUser != null && (
                    <>
                        <input
                            accept='.gsm'
                            className={classes.input}
                            style={{ display: 'none' }}
                            id='file'
                            type='file'
                            onChange={e => this.uploadFile(currentUser, e)}
                        />
                        <label htmlFor='file'>
                            <Button variant='contained' component='span' className={classes.button}>
                                Upload
                                </Button>
                        </label>
                    </>
                )}
            </UserContext.Consumer>);
        return (
            <div className={classes.root}>
                {progress}
                <Paper className={classes.goldsimArea}>
                    <div>
                        <Table>
                            <TableBody>
                                {data.map((row, ri) => (
                                    <TableRow key={ri}>
                                        {row.map((cell, ci) => (
                                            <TableCell key={ri.toString() + ci.toString()}>{cell}</TableCell>
                                        ))}
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </div>
                    <div>
                        {button}
                    </div>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={snackOpen}
                        onClose={this.handleSnackClose}
                        autoHideDuration={6000}>
                        <SnackbarContent
                            className={classes[snackVariant]}
                            aria-describedby="client-snackbar"
                            message={
                                <span id="client-snackbar" className={classes.message}>
                                    {snackMessage}
                                </span>
                            }
                        />
                    </Snackbar>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Assets)