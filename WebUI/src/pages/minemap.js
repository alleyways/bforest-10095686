import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CircularProgress from '@material-ui/core/CircularProgress'
import { blue } from '@material-ui/core/colors';
import Paper from '@material-ui/core/Paper';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Map, TileLayer, GeoJSON } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import * as L from 'leaflet';
import {
    feature as turfFeature,
    centroid as turfCentroid,
    featureCollection as turfFeatureCollection,
    envelope as turfEnvelope,
    bbox as turfBbox,
} from '@turf/turf';

import { getIcon } from '../dhi/Utils';
import { fetchTimeseries, fetchItems, fetchFeatureTypeInfo, fetchFeatureCollectionValues } from '../dhi/DataServices';
import MapSettings from '../components/MapSettings';
const ChartPlotly = React.lazy(() => import('../dhi/Charts/ChartPlotly'));

// fix for leaflet default icon markers
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const styles = theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
        paddingRight: 60,
        flexDirection: 'column',
        fontFamily: theme.fontFamily,
    },
    mapPanel: {
        padding: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        color: theme.palette.text.secondary,
        display: 'flex',
        flexGrow: 1,
        position: 'relative'
    },
    map: {
        flexGrow: 1,
    },
    chartPanel: {
        padding: theme.spacing.unit * 2,
        height: 200,
        color: theme.palette.text.secondary,
        display: 'flex',
        flexDirection: 'column'
    },
    spacerPanel: {
        flexGrow: 1,
    },
    gridContainerFlexed: {
        flexGrow: 1,
    },
    drawerHeader: {
        paddingTop: 64,
        width: 320
    },
    permaDrawerHeader: {
        paddingTop: 64,
        width: 50
    },
    progress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        zIndex: 999
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    }
});

class MineMap extends Component {

    constructor(props) {
        super(props)
        this.state = {
            itemConfiguration: 'MineMap',
            observationPeriodId: '',
            loading: [],
            snackOpen: false,
            snackMessage: '',
            snackVariant: '',
            tableOfContentOpen: false,
            timeseriesData: [],
            featureCollections: [],
            selectedFeature: '',
            selectedLayerId: '',
            selectedThresholds: [],
            chartLayout: {
                autosize: true,
                showlegend: true,
                legend: {
                    //x: 1.1,
                },
                title: '',
                margin: {
                    b: 30,
                    t: 30,
                },
                xaxis: {
                    title: false
                },
            },
            mapSettings: [],
        }
    }

    leafletMap = {};

    handleSettingsEyeClick = (item, layer) => {
        this.featureSelect(item.id, layer.id, true)
    }

    handleSettingsLabelClick = (item, layer) => {
        this.featureSelect(item.id, layer.id)
    }

    toggleDrawer = () => {
        this.setState(state => ({
            tableOfContentOpen: !state.tableOfContentOpen
        }))
    }

    getFeatureFromCollection = (layerId, featureId) => {
        const {
            featureCollections
        } = this.state

        return featureCollections.filter(f => {
            const layerIdPart = f.id.split(';')[0].split('=')[1]
            return layerId === layerIdPart
        })[0].data.features.filter(g => {
            return g.properties.spreadsheetitemid === featureId
        })[0]
    }

    featureSelect = (layerId, featureId, zoomTo = false) => {
        // figure out the centroid for zoomto
        const feature = this.getFeatureFromCollection(layerId, featureId)

        if (zoomTo) {
            const thisFeature = turfFeature(feature.geometry)
            const centroid = turfCentroid(thisFeature)

            const map = this.leafletMap.leafletElement
            map.setView(centroid.geometry.coordinates.reverse(), 16);
        }

        // lookup thresholds
        const thresholdKeys = Object.keys(feature.properties).filter(k => {
            return k.includes('threshold_')
        })

        // group thresholds
        const thresholds = thresholdKeys.map(t => {
            const parts = t.split('_')
            return `${parts[0]}_${parts[1]}`
        })

        const thresholdGroups = Array.from(new Set(thresholds))

        // arrange thresholds
        const arrangedThresholds = thresholdGroups.map(g => {
            const theseThresholds = thresholdKeys.filter(t => {
                return t.includes(g)
            })

            let newThreshold = {}
            theseThresholds.forEach(x => {
                newThreshold[x.split('_')[2]] = feature.properties[x]
            })

            return newThreshold
        })

        // no nulls
        const finalThresholds = arrangedThresholds.filter(t => {
            return t.Value !== null
        })

        this.setState(state => ({
            selectedLayerId: layerId,
            selectedFeature: featureId,
            selectedThresholds: finalThresholds,
        }), () => this.fetchData()) // fetchData is called here so that it is executed syncronously after state update
    }

    onEachFeature = layerId => (feature, layer) => {
        // Tooltip should show description, disabled for now
        // layer.bindTooltip(feature.properties.spreadsheetitemid, {
        //     permanent: true,
        //     direction: 'bottom',
        // })

        const layerIdPart = layerId.split(';')[0].split('=')[1]
        layer.on('click', (e) => this.featureSelect(layerIdPart, e.target.feature.properties.spreadsheetitemid))
    };

    fetchData = () => {
        const tsId = `${this.state.selectedLayerId}/${this.state.selectedFeature}`
        let dataSources = [{
            host: window.config.apiurl,
            connection: window.config.timeseriesConnection,
            // from: '',
            // to: '',
            ids: [
                `Id=${tsId};
                    ConfigurationName=${this.state.itemConfiguration};
                    ThemeId=;
                    ObservationPeriod=${this.state.observationPeriodId};
                    ObservationPeriodOffset=;`,
            ],
        }];

        this.addLoader()
        fetchTimeseries(dataSources).subscribe(
            data => this.handleTimeseries(data),
            () => this.removeLoader('Error fetching timeseries.'),
            () => this.removeLoader()
        );
    };

    handleTimeseries = data => {
        const allYAxes = data.map(d => {
            return {
                id: `${d.data.Metadata.Type}(${d.data.Metadata.Unit})`
            }
        })

        // plotly styling rules:
        // https://images.plot.ly/plotly-documentation/images/plotly_js_cheat_sheet.pdf

        const uniqueYAxes = Array.from(new Set(allYAxes))
        const withYChoice = uniqueYAxes.map((x, index) => {
            return {
                id: x.id,
                seriesYChoice: index === 0 ? 'y' : `y${index + 1}`,
                layoutName: index === 0 ? 'yaxis' : `yaxis${index + 1}`
            }
        })

        // yaxis left or right choice is automatic - Chart Area Index not used
        // add new yaxis objects for each in withYChoice
        const layout = Object.assign({}, this.state.chartLayout)
        layout.title = this.state.selectedFeature
        layout.xaxis.domain = [0.1, 0.95]

        withYChoice.forEach((y, index) => {

            const newAxis = {
                title: y.id
            }

            if (index === 1) {
                newAxis.overlaying = 'y'
                newAxis.side = 'right'
            }

            if (index > 1) {
                newAxis.overlaying = 'y'
                newAxis.side = 'left'
                newAxis.anchor = 'free'
            }

            layout[y.layoutName] = newAxis
        })

        const series = []

        data.forEach(d => {

            const axisLookup = withYChoice.filter(y => {
                return y.id === `${d.data.Metadata.Type}(${d.data.Metadata.Unit})`
            })[0].seriesYChoice

            // plotly has either scatter or bar type
            const type = d.data.Metadata.ChartStyle === 'Column' ? 'bar' : 'scatter'

            // mode can be either lines;lines+markers;markers so multiple checks needed
            // ignored when type === bar 
            let mode = d.data.Metadata.ChartStyle === 'Point' ? 'markers' : 'lines'

            // line color and marker color
            // ignored by the unused type so it's fine to apply both
            const marker = {
                color: d.data.Metadata.Color,
                size: d.data.Metadata.Size
            }
            const line = {
                dash: d.data.Metadata.LineStyle.toLowerCase(),
                color: d.data.Metadata.Color,
                width: d.data.Metadata.Size
            }

            if (!d.data.Data) {
                return false
            }

            const traceConfig = {
                id: d.data.Name,
                type: type,
                mode: mode,
                data: {
                    DateTimes: d.data.Data.map(pair => pair[0]),
                    Values: d.data.Data.map(pair => pair[1])
                },
                line: line,
                marker: marker,
                yaxis: axisLookup
            }

            // plotly stackgroup for area charts
            if (d.data.Metadata.ChartStyle === 'Area') {
                traceConfig.stackgroup = 'one'
            }

            series.push(traceConfig)
        })

        // widening or shortening the chart width depending on how many series added
        const selectedLayout = series.length <= 2 ? 0 : 0.1
        const limitedXDomain = 0.95
        layout.xaxis.domain = [selectedLayout, limitedXDomain]

        // if there are threshold lines included as port of the featurecollection,
        // and if they have values, add them to the chart.
        // It has to be assumed that the first timeseries in the list owns the thresholds,
        // as the information about which timeseries owns the thresholds is not supplied by the API
        const thresholds = this.state.selectedThresholds
        layout.shapes = []

        thresholds.forEach(threshold => {
            const shape = {
                type: 'line',
                xref: 'paper',
                x0: selectedLayout,
                y0: threshold.Value,
                x1: limitedXDomain,
                y1: threshold.Value,
                line: {
                    color: threshold.Color,
                    width: 2,
                    dash: 'dash'
                }
            }

            layout.shapes.push(shape)
        })

        this.setState(state => {
            return {
                chartLayout: layout,
                timeseriesData: series,
            }
        })
    }

    handleFeatureTypeInfo = features => {
        const mapSettings = features.map(f => {
            const layers = Object.values(f.Items).map(i => {
                return {
                    icon: '',
                    name: i.Name,
                    visible: true,
                    id: i.Id,
                }
            })

            return {
                category: f.Name,
                layers: layers,
                style: f.DefaultLayerStyle,
                id: f.Id
            }
        })

        this.setState(state => ({
            mapSettings: mapSettings
        }))
    }

    handleFeatureClasses = features => {

        // turf method for growing bounds
        const envelopes = features.map(f => {
            const turfFeatures = f.data.features.map(g => {
                return turfFeature(g.geometry)
            })

            return turfEnvelope(turfFeatureCollection(turfFeatures))
        })

        const allEnvelope = turfEnvelope(turfFeatureCollection(envelopes.map(e => { return turfFeature(e.geometry) })))
        const bbox = turfBbox(allEnvelope)
        const map = this.leafletMap.leafletElement
        map.fitBounds(
            [
                [bbox[1], bbox[0]],
                [bbox[3], bbox[2]]
            ]
        )

        this.setState(state => ({
            featureCollections: features,
        }))
    }

    // handleFeatureClasses depends on state.mapSettings, so this method needs to be
    // called after handleFeatureTypeInfo
    fetchFeatures = featureTypeIds => {
        // construct a datasource for each feature type ID
        const featureDatasources = featureTypeIds.map(f => {
            return {
                host: window.config.apiurl,
                connection: window.config.featureConnection,
                ids: [`Id=${f};ConfigurationName=${this.state.itemConfiguration};ThemeId=;ObservationPeriod=${this.state.observationPeriodId};ObservationPeriodOffset=;`]
            }
        })

        this.addLoader()
        fetchFeatureCollectionValues(featureDatasources).subscribe(
            data => this.handleFeatureClasses(data),
            () => this.removeLoader('Error fetching features.'),
            () => this.removeLoader()
        )
    }

    handleItems = items => {
        // extract the feature ids and observation periods
        // that belong to the MineMap configuration
        const thisItem = items.filter(item => item['Id'] === this.state.itemConfiguration)[0]
        const thisTheme = thisItem.Metadata.Themes[0]
        const featureTypeIds = thisTheme.FeatureTypeIds
        this.setState(state => ({
            observationPeriodId: thisTheme.ObservationPeriods[0].Id
        }))

        // this is a slow query
        this.addLoader()
        fetchFeatureTypeInfo(
            window.config.apiurl,
            window.config.itemConnection,
            this.state.itemConfiguration,
            this.state.observationPeriodId,
            featureTypeIds)
            .subscribe(
                data => {
                    this.handleFeatureTypeInfo(data)
                    this.fetchFeatures(featureTypeIds)
                },
                () => this.removeLoader('Error fetching featuretypeinfo.'),
                () => this.removeLoader()
            );
    }

    getRandomColor = () => {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    // for styling point geojson
    pointToLayer = featureCollectionId => (point, latlng) => {

        const {
            selectedLayerId,
            selectedFeature
        } = this.state
        const layerId = featureCollectionId.split(';')[0].split('=')[1]

        const style = this.state.mapSettings.filter(m => {
            return m.id === layerId
        })[0].style

        style.selectedColor = 'yellow'
        const pointSelected = selectedLayerId === layerId && selectedFeature === point.properties.spreadsheetitemid

        const iconUrl = getIcon(style.PointType.toLowerCase(), {
            fillColor: pointSelected ? style.selectedColor : style.Color,
            fillOpacity: 1,
            color: style.OuterLineColor,
            opacity: 1,
            weight: 1
        })

        const thisIcon = L.icon({
            iconUrl: iconUrl,
            iconSize: [style.Size, style.Size],
        });

        // for the default marker return L.marker(latlng)
        return L.marker(latlng, {
            icon: thisIcon
        })
    }

    // for styling polygon geojson
    featureStyle = featureCollectionId => feature => {

        const {
            selectedLayerId,
            selectedFeature
        } = this.state

        const layerId = featureCollectionId.split(';')[0].split('=')[1]

        const style = this.state.mapSettings.filter(m => {
            return m.id === layerId
        })[0].style

        style.selectedColor = 'yellow'
        const polygonSelected = selectedLayerId === layerId && selectedFeature === feature.properties.spreadsheetitemid

        return {
            weight: 1,
            color: polygonSelected ? style.selectedColor : style.OuterLineColor,
            opacity: 1,
            fillColor: style.Color,
            fillOpacity: style.LayerOpacity,
        }
    }

    componentDidMount () {
        // fetch the MO items to get the list of feature types
        this.addLoader()
        fetchItems(window.config.apiurl, window.config.itemConnection).subscribe(
            data => this.handleItems(data),
            () => this.removeLoader('Error fetching items.'),
            () => this.removeLoader()
        );
    }

    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            snackOpen: false
        });
    }

    addLoader = () => {
        const loading = Object.assign([], this.state.loading)
        loading.push('loading')
        this.setState(state => ({
            loading: loading
        }))
    }

    removeLoader = (errorMessage = null) => {
        const loading = Object.assign([], this.state.loading)
        loading.pop()
        this.setState(state => ({
            loading: loading
        }))

        if (errorMessage) {
            this.setState(state => ({
                snackMessage: errorMessage,
                snackOpen: true,
                snackVariant: 'error'
            }))
        }
    }

    fallback = () => {
        return (
            <div>Loading...</div>
        );
    }

    render () {
        const {
            classes
        } = this.props

        const {
            featureCollections,
            loading,
            timeseriesData,
            chartLayout,
            tableOfContentOpen,
            mapSettings,
            snackOpen,
            snackMessage,
            snackVariant
        } = this.state;

        const geojson =
            featureCollections.length > 0 &&
            featureCollections.map((fc) => (
                <GeoJSON
                    onEachFeature={this.onEachFeature(fc.id)}
                    data={fc.data}
                    key={Math.random()
                        .toString(36)                           // generating random key causes rerender
                        .substr(2, 5)}                          // on each state update, use fc.id if rerender undesired 
                    style={this.featureStyle(fc.id)}            // only called if feature is polygon
                    pointToLayer={this.pointToLayer(fc.id)}     // only called is feature is point
                >
                </GeoJSON>
            ));

        const progress = loading.length > 0 && (
            <CircularProgress className={classes.progress} />
        )

        const chart = timeseriesData.length > 0 && (
            <React.Suspense fallback={this.fallback()}>
                <ChartPlotly
                    timeseries={timeseriesData}
                    data={timeseriesData}
                    layout={chartLayout} />
            </React.Suspense>
        )

        return (
            <div className={classes.root}>
                {progress}
                <Paper className={classes.mapPanel}>
                    <Map
                        ref={m => {
                            this.leafletMap = m;
                        }}
                        className={classes.map}>
                        <TileLayer url="https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}" />
                        {geojson}
                    </Map>
                </Paper>
                <Paper className={classes.chartPanel}>
                    {chart}
                </Paper>
                <Drawer
                    variant="persistent"
                    anchor="right"
                    open={true}>
                    <div className={classes.permaDrawerHeader}>
                        <IconButton onClick={this.toggleDrawer}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                </Drawer>
                <Drawer
                    variant="persistent"
                    anchor="right"
                    open={tableOfContentOpen}>
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.toggleDrawer}>
                            <ChevronRightIcon />
                        </IconButton>
                    </div>
                    <MapSettings
                        settings={mapSettings}
                        onLabelClick={this.handleSettingsLabelClick}
                        onEyeClick={this.handleSettingsEyeClick}
                    />
                </Drawer>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={snackOpen}
                    onClose={this.handleSnackClose}
                    autoHideDuration={6000}>
                    <SnackbarContent
                        className={classes[snackVariant]}
                        aria-describedby="client-snackbar"
                        message={
                            <span id="client-snackbar" className={classes.message}>
                                {/* <Icon className={classNames(classes.icon, classes.iconVariant)} /> */}
                                {snackMessage}
                            </span>
                        }
                    />
                </Snackbar>
            </div>
        )
    }
}

export default withStyles(styles)(MineMap)