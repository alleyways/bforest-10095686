import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { fetchItems, fetchFeatureTypeInfo } from '../dhi/DataServices';
import MUIDataTable from "mui-datatables";
import CircularProgress from '@material-ui/core/CircularProgress'
import { blue } from '@material-ui/core/colors';

const styles = theme => ({
    root: {
        display: 'flex',
        padding: theme.spacing.unit * 2,
        flexGrow: 1,
        flexDirection: 'column',
        fontFamily: theme.fontFamily,
    },
    assetArea: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        color: theme.palette.dhi.text,
        padding: theme.spacing.unit * 2,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    icon: {
        width: '16px',
        height: '16px'
    },
    progress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        zIndex: 999
    },
})

const DataTable = withStyles({
    responsiveScroll: {
        height: 'calc(100vh - 320px)',
        maxHeight: 'none',
    },
})(MUIDataTable);

class Assets extends Component {
    constructor(props) {
        super(props)
        this.state = {
            itemConfiguration: 'Assets',
            observationPeriodId: '',
            loading: [],
            snackOpen: false,
            snackMessage: '',
            snackVariant: '',
            datas: []
        }
    }
    componentDidMount () {
        // fetch the MO items to get the list of feature types
        this.addLoader()
        fetchItems(window.config.apiurl, window.config.itemConnection).subscribe(
            data => this.handleItems(data),
            () => this.removeLoader('Error fetching items.'),
            () => this.removeLoader()
        );
    }
    handleItems = items => {
        // extract the feature ids and observation periods
        const thisItem = items.filter(item => item['Id'] === this.state.itemConfiguration)[0]
        const thisTheme = thisItem.Metadata.Themes[0]
        const featureTypeIds = thisTheme.FeatureTypeIds
        this.setState(state => ({
            observationPeriodId: thisTheme.ObservationPeriods[0].Id
        }))

        // this is a slow query
        this.addLoader()
        fetchFeatureTypeInfo(
            window.config.apiurl,
            window.config.itemConnection,
            this.state.itemConfiguration,
            this.state.observationPeriodId,
            featureTypeIds)
            .subscribe(
                data => this.handleFeatureTypeInfo(data),
                () => this.removeLoader('Error fetching featuretypeinfo.'),
                () => this.removeLoader()
            );
    }
    handleFeatureTypeInfo = features => {
        let datas = [];
        features.forEach(element => {
            Object.values(element.Items).forEach(item => {
                datas.push({ Image: 'data:image/png;base64,' + element.Base64StringImage, Name: item.Name, Description: item.Description });
            })
        });
        this.setState(state => ({
            datas: datas
        }))
    }
    addLoader = () => {
        const loading = Object.assign([], this.state.loading)
        loading.push('loading')
        this.setState(state => ({
            loading: loading
        }))
    }
    removeLoader = (errorMessage = null) => {
        const loading = Object.assign([], this.state.loading)
        loading.pop()
        this.setState(state => ({
            loading: loading
        }))

        if (errorMessage) {
            this.setState(state => ({
                snackMessage: errorMessage,
                snackOpen: true,
                snackVariant: 'error'
            }))
        }
    }
    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ snackOpen: false });
    }
    render () {
        const { classes } = this.props

        const {
            snackOpen,
            snackMessage,
            snackVariant,
            loading,
            datas
        } = this.state
        const columns = [
            {
                name: "Image",
                label: " ",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <img src={value} className={classes.icon} alt='' />
                        );
                    }
                }
            },
            {
                name: "Name"
            },
            {
                name: "Description",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <a href={value}>{value}</a>
                        );
                    }
                }
            },
        ];
        const options = {
            filterType: 'dropdown',
            selectableRows: 'none',
            rowHover: false,
            rowsPerPage: 10,
            responsive: 'scroll',
            sort: false,
            filter: false,
            search: true,
            print: false,
            download: false,
            viewColumns: false,
        };
        const progress = loading.length > 0 && (
            <CircularProgress className={classes.progress} />
        );
        const table = datas.length > 0 && (<DataTable
            title={""}
            data={datas}
            columns={columns}
            options={options}
        />);
        return (
            <div className={classes.root}>
                {progress}
                <Paper className={classes.assetArea}>
                    <div>
                        {table}
                    </div>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={snackOpen}
                        onClose={this.handleSnackClose}
                        autoHideDuration={6000}>
                        <SnackbarContent
                            className={classes[snackVariant]}
                            aria-describedby="client-snackbar"
                            message={
                                <span id="client-snackbar" className={classes.message}>
                                    {snackMessage}
                                </span>
                            }
                        />
                    </Snackbar>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Assets)