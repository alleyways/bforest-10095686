import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { fetchSpreadsheetUsedRange } from '../dhi/DataServices';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CircularProgress from '@material-ui/core/CircularProgress'
import { blue } from '@material-ui/core/colors';

// Home panel uses flexbox column so
// all children are columns stretched to full height
const styles = theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
        fontFamily: theme.fontFamily,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '100%',
    },
    progress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        zIndex: 999
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    }
});

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            homeLink: '',
            loading: [],
            snackOpen: false,
            snackMessage: '',
            snackVariant: '',
        }
    }
    componentDidMount () {
        this.addLoader();
        fetchSpreadsheetUsedRange(
            window.config.apiurl,
            window.config.spreadSheetConnection,
            'Configuration|5 WebApp', 'WebApp')
            .subscribe(
                data => this.handleSpreadsheet(data),
                () => this.removeLoader('Error fetching spreadsheet'),
                () => this.removeLoader()
            );
    }
    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            snackOpen: false
        });
    }
    addLoader = () => {
        const loading = Object.assign([], this.state.loading)
        loading.push('loading')
        this.setState(state => ({
            loading: loading
        }))
    }

    removeLoader = (errorMessage = null) => {
        const loading = Object.assign([], this.state.loading)
        loading.pop()
        this.setState(state => ({
            loading: loading
        }))

        if (errorMessage) {
            this.setState(state => ({
                snackMessage: errorMessage,
                snackOpen: true,
                snackVariant: 'error'
            }))
        }
    }

    handleSpreadsheet = data => {
        data.forEach(element => {
            if (element[0] === "Home") {
                this.setState(state => ({ homeLink: element[1] }));
            }
        });

    }

    render () {
        const { classes } = this.props;
        const {
            loading,
            homeLink,
            snackOpen,
            snackMessage,
            snackVariant
        } = this.state;
        const progress = loading.length > 0 && (
            <CircularProgress className={classes.progress} />
        )
        return (
            <div className={classes.root}>
                {progress}
                <Paper className={classes.paper}>
                    <iframe
                        title="powerbiframe"
                        width="100%"
                        height="100%"
                        src={homeLink}
                        frameBorder="0"
                        allowFullScreen={false} />
                </Paper>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={snackOpen}
                    onClose={this.handleSnackClose}
                    autoHideDuration={6000}>
                    <SnackbarContent
                        className={classes[snackVariant]}
                        aria-describedby='client-snackbar'
                        message={
                            <span id='client-snackbar' className={classes.message}>
                                {snackMessage}
                            </span>
                        }
                        onClose={this.handleSnackClose}
                    />
                </Snackbar>
            </div>
        )
    }
}

export default withStyles(styles)(Home)