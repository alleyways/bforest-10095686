import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Plot from 'react-plotly.js';

/**
 * @class ChartPlotly
 * @extends {Component}
 */
class ChartPlotly extends Component {
  static propTypes = {
    /**
     * Expected data format that comes from the server
     *
     * [
        {
          id: NCOS/PoB/Observations/Surface Elevation/OuterBar,
          data: [points]
        },
        {
          id: OzSea/MirrorHD/LocationExtracts/PoB/Surface Elevation/OuterBar,
          data: [points]
        },
        {
          id: NCOS/PoB/Observations/Surface Elevation/NW4,
          data: [points]
        },
        {
          id: OzSea/MirrorHD/LocationExtracts/PoB/Surface Elevation/NW4,
          data: [points]
        }
     * ]
    */
    // eslint-disable-next-line react/require-default-props
    data: PropTypes.array,
    // Customising config
    // @url: https://plot.ly/javascript/configuration-options/
    config: PropTypes.object,
    // Customizing layout
    // @url: https://plot.ly/javascript/reference/#layout
    layout: PropTypes.object,
    /**
     * Customizing data series
     * @url: https://plot.ly/javascript/reference/
     * Passed timeseries configs must be in the same order as the data,
     * eg first object in timeseries array will correspond to
     * the first data object specified in the data array:
     */
    timeseries: PropTypes.array,
  };

  state = {
    plotData: [],
    config: {
      responsive: true,
      useShortNames: false,
      displayModeBar: false,
      ...this.props.config,
    },
  };

  componentDidMount() {
    this.formatData();
  }

  componentDidUpdate(props) {
    if (props.data !== this.props.data) {
      this.formatData();
    }
  }

  /**
   * Timeseries representation on the chart
    {
      id: 'Timeseries name',
      type: type (eg scatter, bar, etc),
      x: [array of dates], dates go on the x axis
      y: [array of numbers], values go on the y axis
    }
  */
  defineSeriesFormat = (timeseries, index) => {
    const timeseriesConfig = this.props.timeseries && this.props.timeseries[index];

    return {
      x: [],
      y: [],
      mode: 'points',
      type: 'scatter',
      name: this.state.config.useShortNames ? this.shortenTimeseriesName(timeseries.id) : timeseries.id,
      // If timeseries settings were passed in props, override defaults
      ...timeseriesConfig,
    };
  };

  /**
   * Accepts a timeseries name in the following format:
   * 'WombatMaster/RT_MA/Mineplans/RT_MP_15/CND_1'
   *
   * formats and return the name as the following: CND_1
   */
  shortenTimeseriesName = timeseriesName => timeseriesName.substring(timeseriesName.lastIndexOf('/') + 1);

  formatData = () => {
    // Format timeseries data according to Plotly requirements
    // @url: https://github.com/plotly/react-plotly.js
    let plotData = [];

    if (this.props.data === undefined){
      return
    }

    this.props.data.forEach((timeseries, index) => {
      const series = this.defineSeriesFormat(timeseries, index);

      for (let i = 0; i < timeseries.data.DateTimes.length; i++) {
        series.x.push(timeseries.data.DateTimes[i]);
        series.y.push(timeseries.data.Values[i]);
      }

      plotData = [...plotData, series];
    });

    this.setState({ plotData });
  };

  render() {
    return <Plot
      useResizeHandler
      style={{ width: '100%', height: '100%' }} 
      data={this.state.plotData}
      layout={this.props.layout}
      config={this.state.config}
    />;
  }
}

export default ChartPlotly;
