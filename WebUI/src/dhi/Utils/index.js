/**
 * @param {*} timeseries;
 *
 * Accepts the following format:
 * {
 *  timeseries/1/id: [["time", value], ["time", value], ["time", value]],
 *  timeseries/2/id: [["time", value], ["time", value], ["time", value]],
 * }
 *
 * Returns the following format:
 * [
 *  {
 *    id: timeseries/1/id,
 *    data: [["time", value], ["time", value], ["time", value]],
 *  },
 *  {
 *    id: timeseries/2/id,
 *    data: [["time", value], ["time", value], ["time", value]],
 *  },
 * ]
 *
 */
const formatValues = timeseries =>
  Object.keys(timeseries).map(tsId => ({
    id: tsId,
    data: timeseries[tsId],
  }));

const toDateTimeString = dateString => dateString.replace(new RegExp(':', 'g'), '').split('.')[0];

const debounce = (delay, fn) => {
  let timerId;
  return function(...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      fn(...args);
      timerId = null;
    }, delay);
  };
};

const hexToRGBA = (colorInHex, alpha) => {
  const hex = colorInHex.replace('#', '');
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);
  const opacity = alpha / 100;

  const rgba = `rgba(${r}, ${g}, ${b}, ${opacity})`;

  return rgba;
};

const colorToRGB = color => {
  if (color.indexOf('rgb') > -1) {
    return {
      R: color
        .split('(')[1]
        .split(')')[0]
        .split(',')[0],
      G: color
        .split('(')[1]
        .split(')')[0]
        .split(',')[1],
      B: color
        .split('(')[1]
        .split(')')[0]
        .split(',')[2],
    };
  }

  return {
    R: parseInt(color.substring(1, 3), 16),
    G: parseInt(color.substring(3, 5), 16),
    B: parseInt(color.substring(5, 7), 16),
  };
};

const round = (value, decimals) => {
  if (decimals !== 0) {
    const multiplier = 10 ** decimals;
    return Math.round(value * multiplier) / multiplier;
  }

  return Math.round(value);
};

const passwordStrength = password => {
  let score = 0;

  if (!password) {
    return 0;
  }

  // Length 4 or less
  if (password.length < 5) {
    score += 3;
    // Length between 5 and 7
  } else if (password.length < 8) {
    score += 6;
    // Length between 8 and 15
  } else if (password.length < 16) {
    score += 12;
    // Length 16 or more
  } else {
    score += 18;
  }

  // At least one lower case letter
  if (password.match(/[a-z]/)) {
    score += 1;
  }

  // At least one upper case letter
  if (password.match(/[A-Z]/)) {
    score += 5;
  }

  // At least one number
  if (password.match(/\d+/)) {
    score += 5;
  }

  // At least three numbers
  if (password.match(/(.*[0-9].*[0-9].*[0-9])/)) {
    score += 5;
  }

  // At least one special character
  if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~]/)) {
    score += 5;
  }

  // Aat least two special characters
  if (password.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/)) {
    score += 5;
  }

  // Combinations both upper and lower case
  if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
    score += 2;
  }

  // Both letters and numbers
  if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
    score += 2;
  }

  // Letters, numbers, and special characters
  if (password.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
    score += 2;
  }

  if (score < 16) {
    return 0;
  }

  if (score < 25) {
    return 1;
  }

  if (score < 35) {
    return 2;
  }

  if (score < 45) {
    return 3;
  }

  return 0;
};

const toArray = any => {
  if (!Array.isArray(any)) return [any];

  return any;
};

const isObjectEmpty = object => {
  const keys = Object.keys(object);
  for (let i = 0; i < keys.length; i += 1) return false;

  return true;
};

const getObjectProperty = (objectItem, property, compareValue) => {
  let valid = true;
  const properties = property.split('.');
  let value = objectItem;
  for (let i = 0; i < properties.length; i++) {
    if (properties[i].indexOf('!') >= 0) {
      valid = !valid;
      properties[i] = properties[i].replace('!', '');
    }
    value = value[properties[i]];
  }
  if (compareValue) {
    if (typeof compareValue === 'object') {
      for (let i = 0; i < compareValue.length; i++) {
        if (value === compareValue[i]) {
          return valid;
        }
      }
      return !valid;
    }
    return valid ? value === compareValue : !(value === compareValue);
  }
  return valid ? value : !value;
};

const changeObjectProperty = (objectItem, property, intent) => {
  const properties = property.split('.');
  let value = objectItem;
  const body = [value];
  for (let i = 0; i < properties.length; i++) {
    value = value[properties[i]];
    body.push(value);
  }
  body[properties.length] = intent;
  for (let j = properties.length; j > 0; j--) {
    body[j - 1][properties[j - 1]] = body[j];
  }
  return body[0];
};

const getIcon = (markerType, style) => {
	var path = '';
	switch (markerType) {
		case 'circle':
			path = 'M3,12a9,9 0 1,0 18,0a9,9 0 1,0 -18,0';
			break;
		case 'star':
			path = 'M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z';
			break;
		case 'square':
			path = 'M2 2 L22 2 L22 22 L2 22 Z';
			break;
		case 'triangle':
			path = 'M12 2 L22 23 L2 23 Z';
			break;
		case 'invertedTriangle':
			path = 'M 1.74 1 L 20.26 1 C 20.51 1.02 20.73 1.23 20.86 1.56 C 20.99 1.89 21 2.3 20.89 2.64 L 11.42 20.72 C 11.17 21 10.83 21 10.58 20.72 L 1.11 2.64 C 1 2.3 1.01 1.89 1.14 1.56 C 1.27 1.23 1.49 1.02 1.74 1 Z';
			break;
		case 'cross':
			path = 'M12 1 L12 24 M1 12 L24 12';
			break;
		case 'line':
			var lineWidth = style.lineWidth || 6;
			var top = 12 - lineWidth / 2;
			var bottom = 12 + lineWidth / 2;
			path = 'M2 ' + top + ' L22 ' + top + ' L22 ' + bottom + ' L2 ' + bottom + ' Z';
			break;
		case 'diamond':
			path = 'M12 2 L20 12 L12 22 L4 12 Z';
			break;
		default:
			break;
	}

	var weight = style && style.weight ? style.weight : 2;
	var color = style && style.color ? style.color : '#1976D2';
	var opacity = style && style.opacity ? style.opacity : '1';
	var fillColor = style && style.fillColor ? style.fillColor : '#1976D2';
	var fillOpacity = style && style.fillOpacity ? style.fillOpacity : '0.3';

	var icon =
		'<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="24" height="24"><path ' +
		'stroke="' +
		color +
		'" fill="' +
		fillColor +
		'" fill-opacity="' +
		fillOpacity +
		'" stroke-width="' +
		weight +
		'" stroke-opacity="' +
		opacity +
		'" d="' +
		path +
		'"/></svg>';

	if (style && style.lineType) {
		var lineIcon = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="24" height="24">';
		var pathPrefix =
			'<path ' +
			'stroke="' +
			color +
			'" fill="' +
			fillColor +
			'" fill-opacity="' +
			fillOpacity +
			'" stroke-width="' +
			weight +
			'" stroke-opacity="' +
			opacity;
		switch (style.lineType) {
			case 'Dot':
				lineIcon += pathPrefix + '" d=" M 2 9 L 7 9 L 7 15 L 2 15 L 2 9 Z "/>';
				lineIcon += pathPrefix + '" d=" M 9.5 9 L 14.5 9 L 14.5 15 L 9.5 15 L 9.5 9 Z "/>';
				lineIcon += pathPrefix + '" d=" M 17 9 L 22 9 L 22 15 L 17 15 L 17 9 Z "/>';
				break;
			case 'Dash':
				lineIcon += pathPrefix + '" d=" M 1 9.5 L 10 9.5 L 10 14.5 L 1 14.5 L 1 9.5 Z "/>';
				lineIcon += pathPrefix + '" d=" M 14 9.5 L 23 9.5 L 23 14.5 L 14 14.5 L 14 9.5 Z "/>';
				break;
			case 'DashDot':
				lineIcon += pathPrefix + '" d=" M 0 10 L 7 10 L 7 14 L 0 14 L 0 10 Z "/>';
				lineIcon += pathPrefix + '" d=" M 10 10 L 14 10 L 14 14 L 10 14 L 10 10 L 10 10 Z "/>';
				lineIcon += pathPrefix + '" d=" M 17 10 L 24 10 L 24 14 L 17 14 L 17 10 Z "/>';
				break;
			case 'DashDotDot':
				lineIcon += pathPrefix + '" d=" M 1 10 L 11 10 L 11 14 L 1 14 L 1 10 Z "/>';
				lineIcon += pathPrefix + '" d=" M 13 10 L 17 10 L 17 14 L 13 14 L 13 10 Z "/>';
				lineIcon += pathPrefix + '" d=" M 19 10 L 23 10 L 23 14 L 19 14 L 19 10 Z "/>';
				break;
			case 'Solid':
			default:
				// for any other line type we use the default line icon.
				lineIcon = null;
				break;
		}

		if (lineIcon) {
			lineIcon += '</svg>';
			icon = lineIcon;
		}
	}

	return 'data:image/svg+xml;base64,' + window.btoa(icon);
}

export {
  formatValues,
  toDateTimeString,
  debounce,
  hexToRGBA,
  colorToRGB,
  round,
  passwordStrength,
  toArray,
  isObjectEmpty,
  getObjectProperty,
  changeObjectProperty,
  getIcon,
};
