import React from 'react';
import { shallow } from 'enzyme';

import Login from '../Login';

describe('Login', () => {
  it('displays placeholder message Login', () => {
    const wrapper = shallow(<Login />);
    expect(wrapper.find('Button')).toBeTruthy();
  });
});
