import React, { useState } from 'react';
import { string, func } from 'prop-types';

import { CardContent, Card } from '@material-ui/core';

import LoginForm from './partials/LoginForm';
import ResetPasswordForm from './partials/ResetPasswordForm';

const Login = ({
  host,
  onLogin,
  loginButtonText,
  userNamePlaceholder,
  passwordPlaceholder,
  resetPasswordButtonText,
  resetPasswordUserNamePlaceholder,
}) => {
  const [showResetPassword, setShowResetPassword] = useState(false);

  const togglePasswordResetForm = trueOrFalse => () => setShowResetPassword(trueOrFalse);

  return (
    <Card>
      <CardContent>
        {showResetPassword ? (
          <ResetPasswordForm
            host={host}
            onBackToLogin={togglePasswordResetForm}
            resetPasswordButtonText={resetPasswordButtonText}
            resetPasswordUserNamePlaceholder={resetPasswordUserNamePlaceholder}
            onResetPassword={() => console.log('Reset password is not implemented')}
          />
        ) : (
          <LoginForm
            host={host}
            onLogin={onLogin}
            loginButtonText={loginButtonText}
            userNamePlaceholder={userNamePlaceholder}
            passwordPlaceholder={passwordPlaceholder}
            onResetPassword={togglePasswordResetForm}
          />
        )}
      </CardContent>
    </Card>
  );
};

Login.propTypes = {
  host: string,
  onLogin: func,
  loginButtonText: string,
  userNamePlaceholder: string,
  passwordPlaceholder: string,
  resetPasswordButtonText: string,
  resetPasswordUserNamePlaceholder: string,
};

export default Login;
