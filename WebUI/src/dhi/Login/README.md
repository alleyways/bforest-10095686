Login Component Example:

# Default

```js
import { HOST } from '../Globals';

const onLogin = loggedInUser => sessionStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));

<Login
  host={HOST}
  onLogin={onLogin}
  loginButtonText="Login"
  userNamePlaceholder="Username"
  passwordPlaceholder="Password"
  resetPasswordButtonText="Reset Password"
  resetPasswordUserNamePlaceholder="E-Mail Address or User ID"
/>;
```
