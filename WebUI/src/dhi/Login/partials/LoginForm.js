import React, { useState } from 'react';
import { string, func } from 'prop-types';

import { TextField, Button, CircularProgress, Typography } from '@material-ui/core';

import { login } from '../../DataServices';

const LoginForm = ({ userNamePlaceholder, passwordPlaceholder, loginButtonText, onLogin, host, onResetPassword }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [form, setForm] = useState({
    Id: '',
    Password: '',
  });

  const validate = () => form.Id && form.Password;

  const handleChange = name => ({ target: { value } }) => setForm({ ...form, [name]: value });

  const handleSubmit = e => {
    e.preventDefault();
    if (!validate());

    setLoading(true);
    setError(false);
    login(host, form).subscribe(
      user => {
        setLoading(false);
        const loggedInUser = {
          id: form.Id,
          roles: user.Roles ? user.Roles.split(',').map(role => role.trim()) : [],
          name: user.Name,
          success: true,
          metadata: user.Metadata,
          loggedInDateTime: new Date(),
          password: form.Password,
          email: user.Email ? user.Email : null,
          company: user.Company ? user.Company : null,
          phoneNumber: user.PhoneNumber ? user.PhoneNumber : null,
        };

        onLogin(loggedInUser);
      },
      err => {
        setLoading(false);
        setError(true);
      }
    );
  };

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        required
        fullWidth
        name="id"
        error={error}
        margin="dense"
        value={form.Id}
        variant="outlined"
        onChange={handleChange('Id')}
        helperText={error ? 'Invalid Login' : ''}
        label={userNamePlaceholder || 'Username'}
      />
      <TextField
        required
        fullWidth
        margin="dense"
        type="password"
        variant="outlined"
        value={form.Password}
        onChange={handleChange('Password')}
        label={passwordPlaceholder || 'Username'}
      />
      <Typography align="right" component="div" style={{ marginTop: '5px' }}>
        <Button style={{ marginRight: '5px' }} onClick={onResetPassword(true)}>
          Forgot Password?
        </Button>
        <Button type="submit" color="primary" variant="contained">
          {loading ? <CircularProgress color="inherit" size={24} /> : loginButtonText || 'Login'}
        </Button>
      </Typography>
    </form>
  );
};

LoginForm.propTypes = {
  host: string,
  onLogin: func,
  onResetPassword: func,
  loginButtonText: string,
  userNamePlaceholder: string,
  passwordPlaceholder: string,
};

export default LoginForm;
