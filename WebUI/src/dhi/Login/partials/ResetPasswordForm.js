import React, { useState } from 'react';
import { string, func } from 'prop-types';

import { TextField, Button, CircularProgress, Typography } from '@material-ui/core';

import { resetPassword } from '../../DataServices';

const ResetPasswordForm = ({
  host,
  onBackToLogin,
  onResetPassword,
  resetPasswordUserNamePlaceholder,
  resetPasswordButtonText,
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [form, setForm] = useState({
    Id: '',
    Password: '',
  });

  const validate = () => form.Id && form.Password;

  const handleChange = name => ({ target: { value } }) => setForm({ ...form, [name]: value });

  const handleSubmit = e => {
    e.preventDefault();

    if (!validate());

    setLoading(true);

    // @todo: complete password reset form. Current webAPIDemo does not seem to have this endpoint
    resetPassword(host, form).subscribe(
      json => {
        setLoading(false);
        onResetPassword(json);
      },
      err => {
        setLoading(false);
        setError(err);
      }
    );
  };

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        required
        fullWidth
        margin="dense"
        variant="outlined"
        value={form.Id}
        error={error}
        onChange={handleChange('Id')}
        helperText={error ? 'Account Not Found' : ''}
        label={resetPasswordUserNamePlaceholder || 'E-Mail Address or User ID'}
      />

      <Typography align="right" component="div" style={{ marginTop: '5px' }}>
        <Button style={{ marginRight: '5px' }} onClick={onBackToLogin(false)}>
          Return to Login
        </Button>

        <Button type="submit" color="primary" variant="contained">
          {loading ? <CircularProgress color="inherit" size={24} /> : resetPasswordButtonText || 'Reset Password'}
        </Button>
      </Typography>
    </form>
  );
};

ResetPasswordForm.propTypes = {
  host: string,
  onBackToLogin: func,
  onResetPassword: func,
  resetPasswordButtonText: string,
  resetPasswordUserNamePlaceholder: string,
};

export default ResetPasswordForm;
