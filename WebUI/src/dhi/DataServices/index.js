import { from, of, throwError, forkJoin } from 'rxjs';
import { tap, map, flatMap, mergeMap, catchError } from 'rxjs/operators';

import { formatValues, toDateTimeString, toArray } from '../Utils';

const DEFAULT_OPTIONS = {
    headers: {
        'Content-Type': 'application/json',
    },
};

const fetchUrl = (endPoint, options = {}) => {
    const mergedOptions = {
        ...DEFAULT_OPTIONS,
        ...options,
        headers: {
            ...DEFAULT_OPTIONS.headers,
            ...options.headers,
        },
    };

    return from(fetch(endPoint, mergedOptions)).pipe(
        tap(response => console.log(`Response status: ${response.status}`)),
        map(response => {
            if (response.status >= 400) {
                throw new Error(`Error: ${response.status}, reason: ${response.statusText}`);
            } else {
                return response;
            }
        }),
        flatMap(response => (response.status !== 204 ? response.json() : of(response))),
        catchError(error => throwError(error))
    );
};

const fetchTimeseriesValues = dataSources => {
    const requests = dataSources.map(source => {
        let url = `${source.host}/api/timeseries/${source.connection}/list/values`;
        const params = []

        if (source.from) {
            params.push(`from=${toDateTimeString(source.from)}`)
        }

        if (source.to) {
            params.push(`to=${toDateTimeString(source.to)}`)
        }

        const queryParams = params.join('&')
        if (queryParams.length > 0) {
            url = `${url}?${queryParams}`
        }
        
        return fetchUrl(
            url,
            {
                method: 'POST',
                body: JSON.stringify(source.ids),
            },
            false
        ).pipe(map(timeseries => formatValues(timeseries)));
    });

    return forkJoin(requests).pipe(map(timeseries => [].concat(...timeseries)));
};

const fetchFeatureCollectionValues = dataSources => {
    const requests = dataSources.map(source =>
        fetchUrl(`${source.host}/api/featurecollection/${source.connection}/list`, {
            method: 'POST',
            body: JSON.stringify(source.ids),
        }).pipe(map(fc => formatValues(fc)))
    );

    return forkJoin(requests).pipe(map(fc => fc.flat()));
};

const fetchTimeseriesValuesByGroup = dataSources => {
    const dataSourcesArray = toArray(dataSources);

    // Replace forward slashes with pipes to send as a GET request
    const requests = dataSourcesArray.flatMap(source =>
        source.ids.map(group =>
            fetchUrl(`${source.host}/api/timeseries/${source.connection}/fullnames?group=${group.replace(/\//g, '|')}`, {
                'Content-Type': 'application/json',
            })
        )
    );

    return forkJoin(requests).pipe(tap(ts => console.log('got group names', ts)));
};

const login = (host, user) =>
    fetchUrl(`${host}/api/account/validation`, {
        method: 'POST',
        body: JSON.stringify(user),
    }).pipe(tap(res => console.log('login res', res)));

const resetPassword = (host, user) =>
    fetchUrl(`${host}/api/account/passwordreset`, {
        method: 'POST',
        body: user.Id,
    }).pipe(tap(res => console.log('password reset', res)));

const fetchAccounts = (host, user) =>
    fetchUrl(`${host}/api/account/list`, {
        method: 'GET',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
    }).pipe(tap(res => console.log('fetch accounts', res)));

const deleteAccount = (host, user, usernameToDelete) =>
    fetchUrl(`${host}/api/account/${usernameToDelete}`, {
        method: 'DELETE',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
    }).pipe(tap(res => console.log('deleted account', res)));

const updateAccount = (host, user, userToUpdate) =>
    fetchUrl(`${host}/api/account`, {
        method: 'PUT',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify(userToUpdate),
    }).pipe(tap(json => console.log('updated user', json)));

const createAccount = (host, user, userToCreate) =>
    fetchUrl(`${host}/api/account`, {
        method: 'POST',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify(userToCreate),
    });

const fetchMapAnimationFiles = (dataSources, config) => {
    const dataSourcesArray = toArray(dataSources);
    const source = dataSourcesArray[0];
    const imageSources = [
        {
            [Object.keys(source.ids)[0]]: Object.values(source.ids)[0],
        },
        source.ids,
    ];
    const url = `${source.host}/api/map/${source.connection}/list?&style=${config.style}&item=${config.item}&width=${
        config.width
        }&height=${config.height}&bbox=${config.bbox}&shadingtype=${config.shadingType}&scale=${config.scale}`;

    return from(imageSources).pipe(
        mergeMap(imageSource =>
            fetchUrl(url, {
                method: 'POST',
                body: JSON.stringify(imageSource),
            })
        )
    );
};

const fetchScenario = (dataSource, user, id) =>
    fetchUrl(`${dataSource.host}/api/scenario/${dataSource.connection}/${id}?scenarioId=${id}`, {
        method: 'GET',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
    }).pipe(tap(res => console.log('scenario fetched', res)));

const fetchScenariosList = (dataSource, user) =>
    fetchUrl(`${dataSource.host}/api/scenario/${dataSource.connection}/list`, {
        method: 'GET',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
    }).pipe(tap(res => console.log('scenario list fetched', res)));

const fetchScenariosByDate = (dataSource, user) =>
    fetchUrl(
        `${dataSource.host}/api/scenario/${dataSource.connection}/list?from=${dataSource.from}&to=${dataSource.to}`,
        {
            method: 'GET',
            headers: {
                Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
            },
        }
    );

const deleteScenario = (dataSource, user, id) =>
    fetchUrl(`${dataSource.host}/api/scenario/${dataSource.connection}/${id}`, {
        method: 'DELETE',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
    }).pipe(tap(res => console.log('scenario deleted', res)));

const postScenario = (dataSource, user, scenario) =>
    fetchUrl(`${dataSource.host}/api/scenario/${dataSource.connection}`, {
        method: 'POST',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify(scenario),
    }).pipe(tap(res => console.log('scenario posted', res)));

const updateScenario = (dataSource, user, scenario) =>
    fetchUrl(`${dataSource.host}/api/scenario/${dataSource.connection}`, {
        method: 'PUT',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify(scenario),
    }).pipe(tap(res => console.log('scenario updated', res)));

const executeScenario = (dataSource, user, id) =>
    fetchUrl(`${dataSource.host}/api/job/${dataSource.connection}`, {
        method: 'POST',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify({
            TaskId: 'workflow',
            Parameters: {
                ScenarioId: id,
            },
        }),
    }).pipe(tap(res => console.log('scenario executed', res)));

const fetchTimeseries = dataSources => {
    const requests = dataSources.map(source => {
        let url = `${source.host}/api/timeseries/${source.connection}/list`;

        if (source.from && source.to) {
            url = `${url}?from=${toDateTimeString(source.from)}&to=${toDateTimeString(source.to)}`;
        }

        return fetchUrl(
            url,
            {
                method: 'POST',
                body: JSON.stringify(source.ids),
            },
            false
        ).pipe(map(timeseries => formatValues(timeseries)));
    });

    return forkJoin(requests).pipe(map(timeseries => [].concat(...timeseries)));
};

const updateTimeseries = (host, connection, user, id, data) => {
    let url = `${host}/api/timeseries/${connection}/${id.replace(/\//g, '|')}/values`;

    return fetchUrl(
        url,
        {
            method: 'PUT',
            headers: {
                Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
            },
            body: JSON.stringify(data),
        },
        false
    ).pipe(tap(res => console.log('timeseries updated', res)));
};

const executeJob = (host, connection, user, taskId, parameters) =>
    fetchUrl(`${host}/api/job/${connection}`, {
        method: 'POST',
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        },
        body: JSON.stringify({
            TaskId: taskId,
            Parameters: parameters,
        }),
    }).pipe(tap(res => console.log('job executed', res)));

const fetchJobsByName = (host, connection, taskId) =>
    fetchUrl(`${host}/api/job/${connection}/list?task=${taskId}`, {
        method: 'GET',
    }).pipe(tap(res => console.log('jobs fetched', res)));

const fetchItems = (host, connection) =>
    fetchUrl(`${host}/api/timestep/${connection}/items`, {
        method: 'GET',
    }).pipe(tap(res => console.log('items fetched', res)));

const fetchFeatureTypeInfo = (host, connection, configuration, observationPeriodId, ids) => {
    const url = `${host}/api/timestep/${connection}/ConfigurationName=${
        configuration
        };ObservationPeriodId=${observationPeriodId};ObservationPeriodOffset=;ThemeId=;Type=FeatureTypeInfo;Id=${ids.join('$')}/data/2000-01-01T000000`;

    return fetchUrl(url, {
        method: 'GET',
    }).pipe(tap(res => console.log('FeatureTypeInfo fetched', res)));
}

const fetchSpreadsheetUsedRange = (host, connection, spreadsheet, sheetName) => {
    const url = `${host}/api/spreadsheet/${connection}/${spreadsheet}/${sheetName}/usedrange`;

    return fetchUrl(url, {
        method: 'GET',
    }).pipe(tap(res => console.log('SpreadsheetUsedRange fetched', res)));
}
const uploadDocument = (host, connection, user, file) => {
    fetchUrl(`${host}/api/document/${connection}/${file.name}`, {
        method: 'POST',
        body: file,
        headers: {
            Authorization: `Basic ${btoa(`${user.id}:${user.password}`)}`,
        }
    })
};
export {
    login,
    fetchUrl,
    resetPassword,
    fetchAccounts,
    deleteAccount,
    updateAccount,
    createAccount,
    fetchTimeseriesValues,
    updateTimeseries,
    fetchFeatureCollectionValues,
    fetchTimeseriesValuesByGroup,
    fetchMapAnimationFiles,
    fetchScenario,
    fetchScenariosList,
    fetchScenariosByDate,
    deleteScenario,
    postScenario,
    updateScenario,
    executeScenario,
    fetchTimeseries,
    executeJob,
    fetchJobsByName,
    fetchItems,
    fetchFeatureTypeInfo,
    fetchSpreadsheetUsedRange,
    uploadDocument
};
